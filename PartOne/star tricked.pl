day(tuesday).
day(wednesday).
day(thursday).
day(friday).

mr(nikto).
mr(klatu).
mr(gort).
mr(barrada).

object(balloon).
object(clothesline).
object(frisbee).
object(water_tower).

solve :-
    day(NiktoDay), day(KlatuDay), day(GortDay), day(BarradaDay),
    all_different([NiktoDay, KlatuDay, GortDay, BarradaDay]),
    
    object(NiktoObject), object(KlatuObject),
    object(GortObject), object(BarradaObject),
    all_different([NiktoObject, KlatuObject, GortObject, BarradaObject]),

    Triples = [ [nikto, NiktoDay, NiktoObject],
                [klatu, KlatuDay, KlatuObject],
                [gort, GortDay, GortObject],
                [barrada, BarradaDay, BarradaObject] ],
   
    % 1. Mr.klatu made his sighting at some point earlier in the week than the one who saw the balloon, but at some point later in the week than the one who spotted frisbee(who isn't Mr.gort).
    member([klatu,thursday, _], Triples),
	\+ member([ _,tuesday,balloon], Triples),
	member([ _,thursday, clothesline], Triples),
	member([ _, wednesday, frisbee], Triples),
	\+ member([gort, wednesday, frisbee], Triples),
	
    
    % 2. friday's sighting was made by either Ms.barrada or the one who saw a clothesline (or both).
    (	
		(member([barrada, friday, _], Triples));
		(member([_, friday, clothesline], Triples));
		(member([barrada, friday, clothesline], Triples))),
	
    % 3. Mr.Nitko did not make his sighting on tuesday.
    \+ member([nikto, tuesday, _], Triples),
    
    % 4. Mr.klatu isn't the one whose object turned out to be water_tower.
    \+ member([klatu, _, water_tower], Triples),
    
	
    tell(nikto, NiktoDay, NiktoObject),
    tell(klatu, KlatuDay, KlatuObject),
    tell(gort, GortDay, GortObject),
    tell(barrada, BarradaDay, BarradaObject).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y, Z) :-
    write(' '), write(X), write(' reported on '), write(Y),
    write(' and his UFO is '), write(Z), write('.'), nl.