friend(grizzly_bear).
friend(moose).
friend(seal).
friend(zebra).

mr(joanne).
mr(lou).
mr(ralph).
mr(winnie).

advanture(circus).
advanture(rock_band).
advanture(spaceship).
advanture(train).

solve :-
    friend(JoanneFriend), friend(LouFriend), friend(RalphFriend), friend(WinnieFriend),
    all_different([JoanneFriend, LouFriend, RalphFriend, WinnieFriend]),

    advanture(JoanneAdvanture), advanture(LouAdvanture),
    advanture(RalphAdvanture), advanture(WinnieAdvanture),
    all_different([JoanneAdvanture, LouAdvanture, RalphAdvanture, WinnieAdvanture]),

    Triples = [ [joanne, JoanneFriend, JoanneAdvanture],
                [lou, LouFriend, LouAdvanture],
                [ralph, RalphFriend, RalphAdvanture],
                [winnie, WinnieFriend, WinnieAdvanture] ],

    % 1. The seal (who isn't the creation of either joanne or lou)neither rode to the moon in spaceship nor took a trip around the world on a magic train.
    \+ member([joanne, seal, _], Triples),
	\+ member([lou, seal, _], Triples),
	\+ member([_, seal, spaceship], Triples),
	\+ member([_, seal, train], Triples),

    % 2. joanne's imagenary friend(who isn't the grizzly bear)went to circus.
    \+ member([joanne, grizzly_bear, _], Triples),
    member([joanne, _, circus], Triples),

    % 3. winnie's imagenary friend is zebra.
    member([winnie, zebra, _], Triples),
	
	% 4. The grizzly_bear dint board the spaceship to the moon.
    \+ member([_, grizzly_bear, spaceship], Triples),


    tell(joanne, JoanneFriend, JoanneAdvanture),
    tell(lou, LouFriend, LouAdvanture),
    tell(ralph, RalphFriend, RalphAdvanture),
    tell(winnie, WinnieFriend, WinnieAdvanture).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y, Z) :-
    write('Mr. '), write(X), write(' imagenary friend is '), write(Y),
    write(' whose advanture is '), write(Z), write('.'), nl.




















































