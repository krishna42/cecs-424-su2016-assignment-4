/* Harry potter and the lantern version 2.0 upgraded, by SK.
   Consult this file and issue the command:   start.  */

:- dynamic at/2, i_am_at/1, alive/1.   /* Needed by SWI-Prolog. */
:- retractall(at(_, _)), retractall(i_am_at(_)), retractall(alive(_)).

/* This defines my current location. */

i_am_at(hogwarts).


/* These facts describe how the rooms are connected. */

path(voldemort, d, chamber_of_secrets).

path(chamber_of_secrets, u, voldemort).
path(chamber_of_secrets, w, magical_gate).

path(magical_gate, e, chamber_of_secrets).
path(magical_gate, s, hogwarts).

path(hogwarts, n, magical_gate) :- at(lantern, in_hand).
path(hogwarts, n, magical_gate) :-
        write('Going into that dark chamber_of_secrets without any light?  Are you mad?'), nl,
        !, fail.
path(hogwarts, s, forest).

path(forest, n, hogwarts).
path(forest, w, prison).

path(prison, e, forest).

path(azkaban, w, forest).

path(forest, e, azkaban) :- at(key, in_hand).
path(forest, e, azkaban) :-
        write('The door appears to be locked.'), nl,
        fail.


/* These facts tell where the various objects in the game
   are located. */

at(sorcerers_stone, voldemort).
at(key, magical_gate).
at(lantern, forest).
at(wand, azkaban).


/* This fact specifies that the voldemort is alive. */

alive(voldemort).


/* These rules describe how to pick up an object. */

take(X) :-
        at(X, in_hand),
        write('You''re already holding it!'),
        nl, !.

take(X) :-
        i_am_at(Place),
        at(X, Place),
        retract(at(X, Place)),
        assert(at(X, in_hand)),
        write('OK.'),
        nl, !.

take(_) :-
        write('I don''t see it here.'),
        nl.


/* These rules describe how to put down an object. */

drop(X) :-
        at(X, in_hand),
        i_am_at(Place),
        retract(at(X, in_hand)),
        assert(at(X, Place)),
        write('OK.'),
        nl, !.

drop(_) :-
        write('You aren''t holding it!'),
        nl.


/* These rules define the six direction letters as calls to go/1. */

n :- go(n).

s :- go(s).

e :- go(e).

w :- go(w).

u :- go(u).

d :- go(d).


/* This rule tells how to move in a given direction. */

go(Direction) :-
        i_am_at(Here),
        path(Here, Direction, There),
        retract(i_am_at(Here)),
        assert(i_am_at(There)),
        look, !.

go(_) :-
        write('You can''t go that way.').


/* This rule tells how to look about you. */

look :-
        i_am_at(Place),
        describe(Place),
        nl,
        notice_objects_at(Place),
        nl.


/* These rules set up a loop to mention all the objects
   in your vicinity. */

notice_objects_at(Place) :-
        at(X, Place),
        write('There is a '), write(X), write(' here.'), nl,
        fail.

notice_objects_at(_).


/* These rules tell how to handle killing the buckbeak and the voldemort. */

kill :-
        i_am_at(prison),
        write('Oh, bad luck!  You have just been attacked by a buckbeak.'), nl,
        !, die.

kill :-
        i_am_at(chamber_of_secrets),
        write('You cant do that. You-Know-Who he is He-Who-Must-Not-Be-Named The voldemort it should be the Dark Lord'), nl,
        write('He is  Eternal.').

kill :-
        i_am_at(voldemort),
        at(wand, in_hand),
        retract(alive(voldemort)),
        write('You attacked  voldemorts face repeatedly .  bleeding red'), nl,
        write('pus is coming out of the voldemort''s face, and that typically white-yellow color thing gets all over you.'), nl,
        write('I think you have killed him, despite the continued attack.'),
        nl, !.

kill :-
        i_am_at(voldemort),
        write('Beating on the voldemorts face with your fists has no'), nl,
        write('effect.  This is probably because either you are not strong enough or you dont have a weapon.'), nl.

kill :-
        write('I see nothing suspicious here.'), nl.


/* This rule tells how to die. */

die :-
        !, finish.


/* Under UNIX, the   halt.  command quits Prolog but does not
   remove the output window. On a PC, however, the window
   disappears before the final output can be seen. Hence this
   routine requests the user to perform the final  halt.  */

finish :-
        nl,
        write('The game is over. Please enter the   halt.   command.'),
        nl, !.


/* This rule just writes out game instructions. */

instructions :-
        nl,
        write('Enter commands using standard Prolog syntax.'), nl,
        write('Available commands are:'), nl,
        write('start.                   -- to start the game.'), nl,
        write('n.  s.  e.  w.  u.  d.   -- to go in that direction.'), nl,
        write('take(Object).            -- to pick up an object.'), nl,
        write('drop(Object).            -- to put down an object.'), nl,
        write('kill.                    -- to attack an enemy.'), nl,
        write('look.                    -- to look around you again.'), nl,
        write('instructions.            -- to see this message again.'), nl,
        write('halt.                    -- to end the game and quit.'), nl,
        nl.


/* This rule prints out instructions and tells where you are. */

start :-
        instructions,
        look.


/* These rules describe the various rooms.  Depending on
   circumstances, a room may have more than one description. */

describe(hogwarts) :-
        at(sorcerers_stone, in_hand),
        write('Congratulations!!  You have recovered the sorcerers_stone'), nl,
        write('and won the game.'), nl,
        finish, !.

describe(hogwarts) :-
        write('You are in a hogwarts.  To the north is the chamber door'), nl,
        write('of a chamber_of_secrets; to the south is a thick dense forest.  Your'), nl,
        write('task is, you should decide to accept it, is to'), nl,
        write('recover the magical Azkabans sorcerers_stone and return it to'), nl,
        write('hogwarts.'), nl.

describe(forest) :-
        write('You are in a thick dense forest.  The exit is to the north.'), nl,
        write('There is a barred door to the west, but it seems to be'), nl,
        write('unlocked.  There is a smaller door to the east.'), nl.

describe(prison) :-
        write('You are in a buckbeak''s prison!  The buckbeak has a hungry and'), nl,
        write('terrifying look.  You better get out of here!'), nl.

describe(azkaban) :-
        write('This is nothing but an old city of azkaban.'), nl.

describe(magical_gate) :-
        write('You are in the mouth of a dark chamber_of_secrets.  The exit is to'), nl,
        write('the south; there is a large, dark, round passage to'), nl,
        write('the east.'), nl.

describe(chamber_of_secrets) :-
        alive(voldemort),
        at(sorcerers_stone, in_hand),
        write('The voldemort sees you with the sorcerers_stone and attacks!!!'), nl,
        write('    ...it is over in seconds....'), nl,
        die.

describe(chamber_of_secrets) :-
        alive(voldemort),
        write('voldemort here!  Two faced villan, about the'), nl,
        write('size of a gaint manky, is directly in front of you!'), nl,
        write('I would advise you to leave promptly and quietly....'), nl, !.
describe(chamber_of_secrets) :-
        write('oops!  Here is a giant voldemort here, snooring.'), nl.

describe(voldemort) :-
        alive(voldemort),
        write('You are on top of a giant voldemort, standing upon him'), nl,
        write('dangerous state.  The feel is terrifying.'), nl.

describe(voldemort) :-
        write('Oh, god!  You''re on top of a giant dead voldemort!'), nl.